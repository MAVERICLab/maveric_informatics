Microbiome Informatics (M8161)
==============================

Read on for Microbiology 8161: Microbiome Informatics (Fall 2023). Here, you'll find a list of topics covered by the course
and a set of references (if available) on this website.

Conceptual instructor:

* Prof. Matthew B. Sullivan

Practical instructors:

* Dr. Ahmed Zayed
* Dr. Ben Bolduc
* Dr. Mike Sovic
* Dr. Dean Vik

Teaching assistants

* Dr. Marion Urvoy
* Dr. Ricardo Pavan

**Course learning objectives**: This course seeks to inspire creativity and innovation for answering fundamental
microbiological questions using sequence data. Specific learning objectives include the following:

1. Gain exposure to approaches for studying the function, structure and evolutionary history of genes observed in sequence datasets.
2. Learn approaches for organizing sequence datasets into organismal units using marker genes (e.g., 16S) and shotgun metagenomics data.
3. Learn ecological statistical approaches to discern community structure and ecological drivers from large-scale metagenomic datasets.
4. Introduction to other sequence-based datasets including viral metagenomes, as well as metatranscriptomics, metaproteomics, metabolomics, etc.
5. Design, implement and interpret an informatics group project to further biological understanding of microbes.

**Lecture Schedule**

Week 1: Intro to the Course and OSC (see :ref:`OSC`)

Week 2: OSC and HPC, Review 5161

Week 3: Genome-resolved MetaG + Intro to R for Microbiome Science

Week 4: Introduction to R Parts 2 + 3, Genome-resolved MetaG workflow

Week 5: Ecological stats primer and hands-on applications

Week 6: Select projects and transition to project phase

Week 7: In-class troubleshoot project work (MAGs, DRAM)

Week 8: Project report-outs

Week 9: In-class troubleshoot project work (GTDB for microbial work, virus ID and taxonomy if viruses)

Week 10: Project report-outs

Week 11: Literature context, what are the low-hanging fruit analyses to do?

Week 12: In-class troubleshoot project work (varies upon needs)

Week 13: Project report-outs

Week 14: In-class troubleshoot project work (varies upon needs)

Week 15: Project presentations

Week 16: Project presentations

Finals week: Final paper due!
