.. MAVERIC Informatics documentation master file, created by
   sphinx-quickstart on Sun Sep 15 18:44:24 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MAVERIC Informatics documentation!
=============================================

Welcome to the **M**\icrobial **a**\nd **V**\iral **E**\cological **R**\esearch **i**\n **C**\olumbus Informatics website!

This site serves as a common portal to a variety of resources primarily handled by members of the `Sullivan lab <https://u.osu.edu/viruslab/>`_.

At this site you'll find information relating to:

* The OSU Environmental Microbiology "eMicro" efforts on OSC
* The OSU Microbiome Informatics course (M8161) (more below)
* A public-facing portion of the Sullivan lab's informatics wiki, primarily focused on viral ecology research

Please keep in mind that **this guide is very much a work in progress!**. It is being built, week-by-week, as the microbiome
informatics course is taught during the Fall of 2023. Every week (and often, daily) this site will be updated, built upon
the lectures of M8161, with the goal of providing sufficient information to support class projects and research overall.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Overview
   Microbial-Informatics
   Linux-Unix
   OSC
   QIIME2
   Building-Modules-and-Tools
   eMicro-Apps
   Modules-and-Tools
   R
   Processing-a-Microbial-Metagenome
   Metabolic-and-Pathway-Analyses
   Processing-a-Viral-Metagenome



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
